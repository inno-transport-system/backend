from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissions

from assets.models import *
from assets.serializers import *

__all__ = ['VehicleViewSet']


class VehicleViewSet(viewsets.ModelViewSet):
    permission_classes = (DjangoModelPermissions,)
    serializer_class = VehicleSerializer
    queryset = Vehicle.objects.all()

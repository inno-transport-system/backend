from django.db import models
from djchoices import ChoiceItem, DjangoChoices

__all__ = ['Vehicle']


class Vehicle(models.Model):
    class Status(DjangoChoices):
        broken = ChoiceItem('broken', 'Broken')
        use = ChoiceItem('use', 'In Use')
        garage = ChoiceItem('garage', 'In Garage')

    id = models.AutoField(verbose_name="ID", primary_key=True)

    model_name = models.CharField(verbose_name="Model", max_length=50)
    purchased = models.DateField(verbose_name="Date of purchase")
    mileage = models.FloatField(verbose_name="Mileage in kms", default=0)
    fuel_consumption = models.FloatField(
        verbose_name="Fuel consumption in liters per 100km")

    latitude = models.FloatField(verbose_name="Latitude coordinate", default=0)
    longitude = models.FloatField(
        verbose_name="Longitude coordinate", default=0)

    status = models.CharField(
        verbose_name="Status", choices=Status.choices,
        default=Status.garage, max_length=256)

    carrying_capacity = models.FloatField(
        verbose_name="Carrying capacity in kgs")
    volumetric_capacity = models.FloatField(
        verbose_name="Volumetric capacity in cubic meters")

    def __repr__(self):
        return f'Vehicle {self.model_name} - [{self.purchased}] <{self.id}>'

    def __str__(self):
        return f'{self.model_name} - [{self.purchased}]'

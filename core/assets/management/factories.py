from datetime import date
import factory.fuzzy
from assets.models import *


class VehicleFactory(factory.Factory):
    status = factory.fuzzy.FuzzyChoice(
        choices=[i for i, j in Vehicle.Status.choices])
    model_name = factory.Faker('company')
    purchased = factory.fuzzy.FuzzyDate(
        start_date=date(year=2016, month=1, day=1),
        end_date=date(year=2018, month=10, day=1))
    mileage = factory.fuzzy.FuzzyInteger(low=20000, high=300000)
    fuel_consumption = factory.fuzzy.FuzzyFloat(low=8, high=15)
    carrying_capacity = factory.fuzzy.FuzzyFloat(low=100, high=2000)
    volumetric_capacity = factory.fuzzy.FuzzyFloat(low=10000, high=20000)

    class Meta:
        model = Vehicle

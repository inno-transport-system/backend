import random

from django.core.management.base import BaseCommand

from assets.management.factories import *

vehicle_locations = [(13.404954, 52.520008),  # Germany
                     (2.349014, 48.864716),  # France
                     (37.618423, 55.751244),  # Moscow
                     (4.895168, 52.370216),  # Netherlands
                     (-0.076132, 51.508530)]  # London
pair_of_vehicle_locations = []
for i in range(len(vehicle_locations)):
    for j in range(len(vehicle_locations)):
        if i != j:
            pair_of_vehicle_locations.append((i, j))


class Command(BaseCommand):
    help = 'Generates fake assets'

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        parser.add_argument(
            '--trucks', dest='trucks',
            help='Number of vehicles to generate',
            default=10, type=int)

    def handle(self, trucks, *args, **options):
        vehicles = VehicleFactory.create_batch(size=trucks)
        for truck in vehicles:
            index = random.randint(0, len(pair_of_vehicle_locations) - 1)
            tup = pair_of_vehicle_locations[index]
            truck.source_longitude, truck.source_latitude = vehicle_locations[tup[0]]
            truck.destination_longitude, truck.destination_latitude = vehicle_locations[
                tup[1]]

            truck.save()

from django.test import TestCase
from .models import *
from .serializers import *


# Create your tests here.

class AssetsModelTestCase(TestCase):

    def setUp(self):
        Vehicle.objects.create(model_name="nine", mileage=0, carrying_capacity=45.21,
                               volumetric_capacity=75.1488,
                               fuel_consumption=86.54, purchased="1974-12-03")

    def test_assets_carrying_capacity(self):
        asset = Vehicle.objects.get(model_name="nine")
        self.assertEqual(asset.carrying_capacity, 45.21)

    def test_assets_fuel_consumption(self):
        asset = Vehicle.objects.get(model_name="nine")
        self.assertEqual(asset.fuel_consumption, 86.54)

    def test_assets_purchased_date(self):
        asset = Vehicle.objects.get(model_name="nine")
        self.assertEqual(str(asset.purchased), "1974-12-03")

    def test_assets_volumetric_capacity(self):
        asset = Vehicle.objects.get(model_name="nine")
        self.assertEqual(asset.volumetric_capacity, 75.1488)

    def test_assets_mileage(self):
        asset = Vehicle.objects.get(model_name="nine")
        self.assertEqual(asset.mileage, 0)


class AssetsLocationValidationTestCase(TestCase):

    def test_latitude_validation(self):
        ser = VehicleSerializer(self)
        self.assertEqual(ser.validate_latitude(-48), -48)

    def test_longitude_validation(self):
        ser = VehicleSerializer(self)
        self.assertEqual(ser.validate_longitude(94), 94)

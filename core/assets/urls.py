from rest_framework.routers import DefaultRouter

from assets.views import *

app_name = 'assets'

router = DefaultRouter()
router.register(r'', VehicleViewSet, base_name='vehicle')
urlpatterns = router.urls

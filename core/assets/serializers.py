from rest_framework import serializers

from assets.models import *

__all__ = ['VehicleSerializer']


class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = ('id', 'model_name', 'purchased', 'mileage',
                  'fuel_consumption', 'latitude', 'longitude',
                  'status', 'carrying_capacity', 'volumetric_capacity')
        extra_kwargs = {
            'id': {'read_only': True},
        }

    @staticmethod
    def validate_latitude(value):
        if abs(value) > 90:
            raise serializers.ValidationError(
                "Latitude must be in range [-90;90].")
        return value

    @staticmethod
    def validate_longitude(value):
        if abs(value) > 180:
            raise serializers.ValidationError(
                "Latitude must be in range [-180;180].")
        return value

import factory.fuzzy
from django.contrib.auth.models import User

charset = {
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
}


class UserFactory(factory.Factory):
    username = factory.fuzzy.FuzzyText(length=5, prefix="user", chars=charset)
    password = factory.fuzzy.FuzzyText(length=5, prefix="user", chars=charset)
    email = factory.fuzzy.FuzzyText(length=7, suffix="@kek.com", chars=charset)

    class Meta:
        model = User

from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group, Permission
from django.db.models import Q


class Command(BaseCommand):
    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)

    def handle(self, *args, **options):
        driver_permissions = Permission.objects.filter(
            Q(codename='change_dispatchorder') | Q(codename='view_dispatchorder') | Q(codename='view_vehicle'))

        operator_permissions = Permission.objects.filter(
            Q(codename__contains='_order') | Q(codename__contains='_dispatchorder') | Q(codename__contains='_vehicle'))

        operator = Group(name='Operator')
        driver = Group(name='Driver')

        operator.save()
        driver.save()

        operator.permissions.set(operator_permissions)
        driver.permissions.set(driver_permissions)

from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group

from employees.management.factories import *


class Command(BaseCommand):
    help = 'Generates fake users'

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        parser.add_argument(
            '--drivers', dest='drivers',
            help='Number of drivers to generate',
            default=10, type=int)
        parser.add_argument(
            '--operators', dest='operators',
            help='Number of operators to generate',
            default=3, type=int)

    def handle(self, drivers, operators, *args, **options):
        operator = Group.objects.get(name="Operator")
        driver = Group.objects.get(name="Driver")

        users = UserFactory.create_batch(size=drivers)
        for user in users:
            user.save()
            user.groups.add(driver)
            user.save()

        users = UserFactory.create_batch(size=operators)
        for user in users:
            user.save()
            user.groups.add(operator)
            user.save()

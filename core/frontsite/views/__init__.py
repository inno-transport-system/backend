from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required, permission_required

from assets.models import Vehicle
from frontsite.forms import MultipleDispatchOrdersForm, OrderValidationForm, OrderValidationFormById
from orders.models import *
from assets.models import *
from . import user
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

import math

__all__ = (
    'index', 'user',
    'dispatch_order', 'dispatch_orders', 'create_dispatch',
    'order', 'orders_accordion', 'drivers', 'vehicles',
    'dispatch_orders_for_map', 'stats_kilometers'
)


@login_required
def index(request):
    return render(request, 'index.html')


@login_required
@permission_required('orders.view_order')
def orders_accordion(request):
    orders_all = Order.objects.all()
    paginator = Paginator(orders_all, 10)
    cur_page = int(request.GET.get('page', 1))  # TODO тут кидается 500 если не инт
    start = paginator.page_range.start
    end = paginator.page_range.stop
    orders_page = paginator.get_page(cur_page)

    if request.method == 'POST':
        form = OrderValidationFormById(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = OrderValidationFormById()

    return render(request, 'orders-accordion.html', context=dict(
        orders=orders_page, form=form,
        current_page=cur_page, start_page=start, end_page=end))


@login_required
@permission_required('assets.view_vehicle')
def vehicles(request):
    vehicle = [{'id': '18',
                'model': 'Batmobile 56',
                'status': 'G',
                'carrying_capacity': '1488',
                'volumetric_capacity': '1337',
                'latitude': '55.753598',
                'longitude': '48.743760',
                'fuel_consumption': '12.46',
                'mileage': '9192631770',
                'purchased': '17.01.1998'}]
    return render(request, 'vehicles.html', context={'vehicles': vehicle})


@login_required
@permission_required('orders.view_dispatchorder')
def dispatch_orders(request):
    dispatch_orders_all = DispatchOrder.objects.all().order_by('-creation_time')
    paginator = Paginator(dispatch_orders_all, 10)

    # TODO тут кидается 500 если не инт
    cur_page = int(request.GET.get('page', 1))
    start = paginator.page_range.start
    end = paginator.page_range.stop
    dispatch_orders_page = paginator.get_page(cur_page)
    return render(request, 'dispatch_orders.html', context=dict(
        dispatch_orders=dispatch_orders_page,
        current_page=cur_page, start_page=start, end_page=end))


@login_required
def drivers(request):
    drivers_all = User.objects.filter(groups__name="Driver")
    paginator = Paginator(drivers_all, 10)

    cur_page = int(request.GET.get('page', 1))  # TODO тут кидается 500 если не инт
    start = paginator.page_range.start
    end = paginator.page_range.stop
    drivers_page = paginator.get_page(cur_page)
    return render(request, 'drivers.html', context=dict(
        drivers=drivers_page,
        current_page=cur_page, start_page=start, end_page=end))


@login_required
@permission_required('orders.view_order')
@permission_required('orders.view_dispatchorder')
def order(request, pk):
    order_object = get_object_or_404(Order, id=pk)
    if request.method == 'POST':
        form = OrderValidationForm(order_object, request.POST)
        if form.is_valid():
            form.save()
            return redirect('order', pk=pk)
    else:
        form = OrderValidationForm(order_object)
    disp_orders = order_object.dispatchorder_set.all()

    return render(request, 'order.html', {'order': order_object, 'dispatch_orders': disp_orders, 'form': form})


@login_required
@permission_required('orders.view_dispatchorder')
def dispatch_order(request, pk):
    disp_order = get_object_or_404(DispatchOrder, id=pk)
    return render(request, 'dispatch_order.html', {'dispatch_order': disp_order})


@login_required
@permission_required('orders.add_dispatchorder')
@permission_required('assets.view_vehicle')
def create_dispatch(request, pk):
    order_object = get_object_or_404(Order, id=pk)
    if request.method == 'POST':
        form = MultipleDispatchOrdersForm(order_object, request.POST)
        if form.is_valid():
            form.save()
            return redirect('order', pk=pk)
    else:
        form = MultipleDispatchOrdersForm(order_object)
    vehicles = Vehicle.objects.all()
    return render(request, 'dispatch-order-creation.html', {'order': order_object, 'form': form, 'vehicles': vehicles})


@login_required
@permission_required('assets.view_vehicle')
@permission_required('orders.view_dispatchorder')
def dispatch_orders_for_map(request):  # change here for generations
    dis_orders = DispatchOrder.objects.all()
    vehicles_all = Vehicle.objects.all()

    paginator = Paginator(vehicles_all, 10)
    cur_page = int(request.GET.get('page', 1))  # TODO тут кидается 500 если не инт
    start = paginator.page_range.start
    end = paginator.page_range.stop
    vehicles_page = paginator.get_page(cur_page)

    return render(request, 'vehicles.html', context=dict(
        dispatch_orders=dis_orders, vehicles=vehicles_page,
        current_page=cur_page, start_page=start, end_page=end))


@login_required
def stats_kilometers(request):
    earth_radius = 6371e3
    all_orders = Order.objects.all()
    class_a = []
    class_b = []
    class_c = []
    for cur_order in all_orders:
        phi_1 = cur_order.source_latitude
        phi_2 = cur_order.destination_latitude

        d_lam = cur_order.source_longitude - cur_order.destination_longitude
        phi_1 = phi_1 / 180.0 * math.pi
        phi_2 = phi_2 / 180.0 * math.pi
        d_phi = phi_1 - phi_2
        d_lam = d_lam / 180.0 * math.pi
        a = (math.sin(d_phi / 2) ** 2) + math.cos(phi_1) * \
            math.cos(phi_2) * (math.sin(d_lam / 2) ** 2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
        d = earth_radius * c
        d /= 1000.0
        if d <= 20:
            class_a.append(cur_order)
        elif d <= 400:
            class_b.append(cur_order)
        else:
            class_c.append(cur_order)

    number_of_orders = {}

    for cur_order in all_orders:
        date = cur_order.delivery_window_start
        converted = date.strftime("%d/%m/%y")
        if converted in number_of_orders.keys():
            number_of_orders[converted] += 1
        else:
            number_of_orders[converted] = 1

    nvp_stat = dict()
    nvp_stat["new"] = []
    nvp_stat["validated"] = []
    nvp_stat["planned"] = []

    tmp = dict()
    tmp["new"] = [(int(cur_ord.delivery_window_start.strftime("%y")),
                   int(cur_ord.delivery_window_start.strftime("%m")),
                   int(cur_ord.delivery_window_start.strftime("%d")))
                  for cur_ord in all_orders if cur_ord.status == "new"]
    tmp["validated"] = [(int(cur_ord.delivery_window_start.strftime("%y")),
                         int(cur_ord.delivery_window_start.strftime("%m")),
                         int(cur_ord.delivery_window_start.strftime("%d")))
                        for cur_ord in all_orders if cur_ord.status == "validated"]
    tmp["planned"] = [(int(cur_ord.delivery_window_start.strftime("%y")),
                       int(cur_ord.delivery_window_start.strftime("%m")),
                       int(cur_ord.delivery_window_start.strftime("%d")))
                      for cur_ord in all_orders if cur_ord.status == "planned"]
    for status in tmp.keys():
        dic = {}
        for tup in tmp[status]:
            if tup in dic.keys():
                dic[tup] += 1
            else:
                dic[tup] = 1
        tmp[status] = dic

    for status in tmp.keys():
        for key in tmp[status].keys():
            nvp_stat[status].append(
                {"date": {"year": key[0], "month": key[1], "day": key[2]}, "count": tmp[status][key]})

    return render(request, 'statistics.html',
                  {'NVPstat': nvp_stat,
                   'stats_kilometers': {'A': len(class_a), 'B': len(class_b), 'C': len(class_c)},
                   'statsdays': number_of_orders})

    # return render(request, 'statistics.html', {'stats_kilometers': {'A': 63, 'B': 98, 'C': 24},
    #                                            'NVPstat': {'new': [{'date': {'year': 2018,
    #                                                                          'month': 1,
    #                                                                          'day': 15},
    #                                                                 'count': 15},
    #                                                                {'date': {'year': 2018,
    #                                                                          'month': 2,
    #                                                                          'day': 15},
    #                                                                 'count': 56},
    #                                                                {'date': {'year': 2018,
    #                                                                          'month': 3,
    #                                                                          'day': 15},
    #                                                                 'count': 42}],
    #                                                        'validated': [{'date': {'year': 2018,
    #                                                                                'month': 1,
    #                                                                                'day': 15},
    #                                                                       'count': 23},
    #                                                                      {'date': {'year': 2018,
    #                                                                                'month': 2,
    #                                                                                'day': 15},
    #                                                                       'count': 64},
    #                                                                      {'date': {'year': 2018,
    #                                                                                'month': 3,
    #                                                                                'day': 15},
    #                                                                       'count': 56}],
    #                                                        'planned': [{'date': {'year': 2018,
    #                                                                              'month': 1,
    #                                                                              'day': 15},
    #                                                                     'count': 37},
    #                                                                    {'date': {'year': 2018,
    #                                                                              'month': 2,
    #                                                                              'day': 15},
    #                                                                     'count': 86},
    #                                                                    {'date': {'year': 2018,
    #                                                                              'month': 3,
    #                                                                              'day': 15},
    #                                                                     'count': 48}]
    #                                                        },
    #
    #                                            'statsdays': {'2018/9/30': 31,
    #                                                          '2018/10/1': 15,
    #                                                          '2018/10/2': 14,
    #                                                          '2018/10/5': 2,
    #                                                          '2018/10/3': 26,
    #                                                          '2018/10/4': 42,
    #                                                          '2018/11/4': 56}
    #                                            })

from django.shortcuts import render

__all__ = ('profile',)


def profile(request):
    return render(request, 'user/profile.html')

from django import forms

from assets.models import Vehicle
from orders.models import *


class MultipleDispatchOrdersForm(forms.Form):
    def __init__(self, order, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['errors'] = forms.CharField(required=False)
        self.initial['errors'] = ""
        self.order = order
        self.num_of_fields = 10  # number of fields per dispatch order

    def clean(self):
        for i in range(len(self.data) // self.num_of_fields):
            vehicle = self.data['vehicle_%d' % i]
            src_lng = self.data['source_longitude_%d' % i]
            src_lat = self.data['source_latitude_%d' % i]
            dest_lng = self.data['destination_longitude_%d' % i]
            dest_lat = self.data['destination_latitude_%d' % i]
            dws = self.data['delivery_window_start_%d' % i]
            dwe = self.data['delivery_window_end_%d' % i]
            source = self.data['source_%d' % i]
            destination = self.data['destination_%d' % i]
            deliver_to = self.data['deliver_to_%d' % i]

            donum = 'Dispatch Order #%d: ' % i

            if vehicle is int and Vehicle.objects.filter(id=vehicle).first() is None:
                msg = donum + "Vehicle with ID=%d wasn't found." % vehicle
                self.add_error('errors', msg)

            if src_lng is float and not (-180.0 <= src_lng <= 180.0):
                msg = donum + "Source longitude violates the range of valid values."
                self.add_error('errors', msg)

            if src_lat is float and not (-90.0 <= src_lat <= 90.0):
                msg = donum + "Source latitude violates the range of valid values."
                self.add_error('errors', msg)

            if dest_lng is float and not (-180.0 <= dest_lng <= 180.0):
                msg = donum + "Destination longitude violates the range of valid values."
                self.add_error('errors', msg)

            if dest_lat is float and not (-90.0 <= dest_lat <= 90.0):
                msg = donum + "Destination latitude violates the range of valid values."
                self.add_error('errors', msg)

            if src_lng == dest_lng and src_lat == dest_lat:
                msg = donum + "Source can't be equal to Destination."
                self.add_error('errors', msg)

            if dws >= dwe:
                msg = donum + "The end of the delivery window should be after the start."
                self.add_error('errors', msg)

            self.cleaned_data['vehicle_%d' % i] = vehicle
            self.cleaned_data['source_longitude_%d' % i] = src_lng
            self.cleaned_data['source_latitude_%d' % i] = src_lat
            self.cleaned_data['destination_longitude_%d' % i] = dest_lng
            self.cleaned_data['destination_latitude_%d' % i] = dest_lat
            self.cleaned_data['delivery_window_start_%d' % i] = dws
            self.cleaned_data['delivery_window_end_%d' % i] = dwe
            self.cleaned_data['source_%d' % i] = source
            self.cleaned_data['destination_%d' % i] = destination
            self.cleaned_data['deliver_to_%d' % i] = deliver_to

        print("Cleaning ended.")

    def save(self):
        n = len(self.cleaned_data) // self.num_of_fields
        if n > 0:
            Order.objects.filter(id=self.order.id).update(status="planned")
        for i in range(n):
            DispatchOrder.objects.create(
                status='assigned',
                order=self.order,
                vehicle=Vehicle.objects.filter(
                    id=self.cleaned_data['vehicle_%d' % i]).first(),
                source_longitude=self.cleaned_data['source_longitude_%d' % i],
                source_latitude=self.cleaned_data['source_latitude_%d' % i],
                destination_longitude=self.cleaned_data['destination_longitude_%d' % i],
                destination_latitude=self.cleaned_data['destination_latitude_%d' % i],
                delivery_window_start=self.cleaned_data['delivery_window_start_%d' % i],
                delivery_window_end=self.cleaned_data['delivery_window_end_%d' % i],
                source=self.cleaned_data['source_%d' % i],
                destination=self.cleaned_data['destination_%d' % i],
                deliver_to=self.cleaned_data['deliver_to_%d' % i],
            )

    def get_errors(self):
        result = self.errors.get('errors')
        return [] if result is None else result


class OrderValidationForm(forms.Form):
    def __init__(self, order, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.order = order

    def save(self):
        Order.objects.filter(id=self.order.id).update(status="validated")


class OrderValidationFormById(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['order_id'] = forms.IntegerField()

    def save(self):
        Order.objects.filter(id=self.cleaned_data['order_id']).update(
            status="validated")

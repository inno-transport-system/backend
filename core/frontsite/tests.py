from datetime import datetime, timedelta

from django.urls import reverse
from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework import status

from orders.models import Order
from orders.models import DispatchOrder


class OrderTest(TestCase):
    def setUp(self):
        self.order = Order.objects.create(
            status=Order.Status.validated,
            delivery_window_start=datetime.now() - timedelta(days=2),
            delivery_window_end=datetime.now() + timedelta(days=2))
        self.dispatch = DispatchOrder.objects.create(order=self.order)
        self.user = User.objects.create_superuser(
            username='test', email='putin@navalny.trump', password='test_pwd')
        self.client.login(username='test', password='test_pwd')

    def test_order_200(self):
        url = reverse('order', args=[self.order.id])
        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_order_404(self):
        url = reverse('order', args=[1337])
        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_accordion(self):
        url = reverse('orders')
        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_create_dispatch_order_200(self):
        url = reverse('order-create-dispatches', args=[self.order.id])
        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_create_dispatch_order_404(self):
        url = reverse('order-create-dispatches', args=[1337])
        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

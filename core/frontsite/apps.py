from django.apps import AppConfig


class FrontsiteConfig(AppConfig):
    name = 'frontsite'

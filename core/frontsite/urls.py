from django.urls import path
from django.contrib.auth import views as auth_views

from .views import *

urlpatterns = [
    path('', index, name='homes'),
    path('orders', orders_accordion, name='orders'),
    path('vehicles-list', dispatch_orders_for_map, name='vehicle-list'),
    path('dispatch-orders', dispatch_orders, name='dispatch-orders'),
    path('drivers-list', drivers, name='drivers-list'),
    path('order/<int:pk>', order, name='order'),
    path('order/<int:pk>/create-dispatch',
         create_dispatch, name='order-create-dispatches'),
    path('statistics', stats_kilometers, name='statistics'),
    path('dispatch-order/<int:pk>', dispatch_order, name='dispatch-order'),

    path('user/logout',
         auth_views.LogoutView.as_view(next_page='/'),
         name='user-logout'),
    path('user/login',
         auth_views.LoginView.as_view(template_name='user/login.html'),
         name='user-login'),
    path('user/profile', user.profile, name='user-profile'),
]

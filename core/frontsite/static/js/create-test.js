// 55.698 37.7704
// 55.702 37.7928
// Both Moscow
// 55.6835 37.6634
// 55.7357 37.6215

content = {
  status: 'new',
  weight: 1000,
  number_of_objects: 3,
  source_longitude: 55.698,
  source_latitude: 37.7704,
  destination_longitude: 55.6835,
  destination_latitude: 37.6634,
  delivery_window_start: '2018-10-17T04:15',
  delivery_window_end: '2018-10-27T14:35'
}

$.ajax({
	url: '/api/v1/orders/',
	type: 'POST',
	data: JSON.stringify(content),
	dataType: 'json',
	contentType: 'application/json; charset=utf-8',
})

content['status'] = 'validated'
$.ajax({
	url: '/api/v1/orders/',
	type: 'POST',
	data: JSON.stringify(content),
	dataType: 'json',
	contentType: 'application/json; charset=utf-8',
})


// Generation of validated order

content['source_longitude'] = 37.6634
content['source_latitude'] = 55.6835
content['destination_longitude'] = 37.6215
content['destination_latitude'] = 55.7357 
content['status'] = 'planned'

var created_id = ''
$.ajax({
	url: '/api/v1/orders/',
	type: 'POST',
	data: JSON.stringify(content),
	dataType: 'json',
	contentType: 'application/json; charset=utf-8',
}).done((data) => {
	delete content['number_of_objects']
	delete content['weight']
	content['order'] = data.id
	content['vehicle'] = null
	content['status'] = 'assigned'
	content['source'] = 'Abakan, Prospekt Lenina 75, kv. 14'
	content['destination'] = 'Abakan, Park Gorkova, dom 16'

	$.ajax({
		url: '/api/v1/orders/dispatch/',
		type: 'POST',
		data: JSON.stringify(content),
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
	});
});


// '/api/v1/orders/dispatch/'




// Alter status to validated
var order_id = 'some_id'

$.ajax({
	url: '/api/v1/orders/' + order_id + '/',
	type: 'PATCH',
	data: JSON.stringify({status: 'validated'}),
	dataType: 'json',
	contentType: 'application/json; charset=utf-8',
});
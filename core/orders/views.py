from rest_framework import viewsets
from orders.models import *
from orders.serializers import *
from rest_framework.permissions import DjangoModelPermissions
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from rest_framework.response import Response
import copy


__all__ = ['OrderViewSet', 'DispatchOrderViewSet', 'customer_orders']


class CustomOrderPermission(DjangoModelPermissions):
    def __init__(self):
        # you need deepcopy when you inherit a dictionary type
        self.perms_map = copy.deepcopy(self.perms_map)
        del self.perms_map['POST']

    def has_permission(self, request, view):
        if request.method == 'POST':
            return True

        return super().has_permission(request, view)


class OrderViewSet(viewsets.ModelViewSet):
    permission_classes = (CustomOrderPermission,)
    serializer_class = OrderSerializer
    queryset = Order.objects.all()


class DispatchOrderViewSet(viewsets.ModelViewSet):
    permission_classes = (DjangoModelPermissions,)
    serializer_class = DispatchOrderSerializer
    queryset = DispatchOrder.objects.all()


@csrf_exempt
@api_view(["GET"])
@permission_classes((AllowAny,))
def customer_orders(request, id):
    objs = Order.objects.filter(customer_id=id)
    ans = [{} for i in range(len(objs))]
    for index, order in zip(range(len(objs)), objs):
        d = {}
        d['creation_time'] = order.creation_time
        d['id'] = order.id
        d['weight'] = order.weight
        d['source'] = order.source
        d['destination'] = order.destination
        d['number_of_objects'] = order.number_of_objects
        ans[index] = d
    return Response({'orders': ans}, status=HTTP_200_OK)

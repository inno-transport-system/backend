import random

from django.core.management.base import BaseCommand

from assets.management.factories import *
from orders.management.factories import *


towns = [(13.404954, 52.520008),  # Germany
         (2.349014, 48.864716),  # France
         (37.618423, 55.751244),  # Moscow
         (4.895168, 52.370216),  # Netherlands
         (-0.076132, 51.508530)]  # London
pair_of_towns = []
for i in range(len(towns)):
    for j in range(len(towns)):
        if i != j:
            pair_of_towns.append((i, j))
addresses = ["Baker Street,", "Lenin Street,", "Pushkin Street,"]
recipients = ["Andrey", "Radik", "Pavlik", "Andruysha", "Oleksandr"]


class Command(BaseCommand):
    help = 'Generates fake data'

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        parser.add_argument(
            '--trucks', dest='trucks',
            help='Number of vehicles to generate',
            default=3, type=int)
        parser.add_argument(
            '--orders', dest='orders',
            help='Number of orders to generate',
            default=10, type=int)
        parser.add_argument(
            '--dispatches-min', dest='dispatches_min',
            help='Minimum number of dispatch orders per planned order',
            default=4, type=int)
        parser.add_argument(
            '--dispatches-max', dest='dispatches_max',
            help='Maximum number of generated pins per chan',
            default=10, type=int)

    def handle(self, trucks, orders, dispatches_min, dispatches_max, *args, **options):
        vehicles = VehicleFactory.create_batch(size=trucks)
        for vehicle in vehicles:
            vehicle.save()
        veh_list = list(Vehicle.objects.all())
        orders = OrderFactory.create_batch(size=orders)
        for order in orders:
            index = random.randint(0, len(pair_of_towns) - 1)
            tup = pair_of_towns[index]

            order.source_longitude, order.source_latitude = towns[tup[0]]
            order.destination_longitude, order.destination_latitude = towns[tup[1]]

            order.customer_id = 1
            order.source = addresses[random.randint(0, len(addresses) - 1)]
            order.source += str(random.randint(1, 100))
            order.destination = addresses[random.randint(
                0, len(addresses) - 1)]
            order.destination += str(random.randint(1, 100))
            order.save()

            if order.status not in (Order.Status.planned, Order.Status.delivered):
                continue

            dispatches_num = random.randint(dispatches_min, dispatches_max)
            for _ in range(dispatches_num):
                veh = veh_list[random.randint(0, len(veh_list) - 1)]
                dispatch = DispatchOrderFactory.create(
                    order=order, vehicle=veh)
                dispatch.source = addresses[random.randint(
                    0, len(addresses) - 1)]
                dispatch.destination = addresses[random.randint(
                    0, len(addresses) - 1)]
                dispatch.source += str(random.randint(1, 100))
                dispatch.destination += str(random.randint(1, 100))
                dispatch.deliver_to = recipients[random.randint(
                    0, len(recipients) - 1)]
                dispatch.save()

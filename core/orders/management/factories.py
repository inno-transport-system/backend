import factory.fuzzy
import datetime
from pytz import UTC
from orders.models import *


class OrderFactory(factory.Factory):
    status = factory.fuzzy.FuzzyChoice(
        choices=[i for i, j in Order.Status.choices])
    weight = factory.fuzzy.FuzzyFloat(low=0, high=10000.0)  # in grams
    number_of_objects = factory.fuzzy.FuzzyInteger(1, 100)
    creation_time = factory.fuzzy.FuzzyDateTime(
        datetime.datetime(2017, 1, 1, tzinfo=UTC))

    delivery_window_start = factory.fuzzy.FuzzyDateTime(
        datetime.datetime(2017, 1, 1, tzinfo=UTC),
        datetime.datetime(2018, 1, 1, tzinfo=UTC))
    delivery_window_end = factory.fuzzy.FuzzyDateTime(
        datetime.datetime(2018, 1, 1, tzinfo=UTC),
        datetime.datetime(2018, 10, 10, tzinfo=UTC))

    class Meta:
        model = Order


class DispatchOrderFactory(factory.Factory):
    source_latitude = factory.fuzzy.FuzzyFloat(low=55.725152, high=55.782366)
    source_longitude = factory.fuzzy.FuzzyFloat(low=48.707519, high=48.793293)
    destination_latitude = factory.fuzzy.FuzzyFloat(
        low=55.725152, high=55.782366)
    destination_longitude = factory.fuzzy.FuzzyFloat(
        low=48.707519, high=48.793293)
    creation_time = factory.fuzzy.FuzzyDateTime(
        datetime.datetime(2017, 1, 1, tzinfo=UTC))

    status = factory.fuzzy.FuzzyChoice(
        choices=[i for i, j in DispatchOrder.Status.choices])
    delivery_window_start = factory.fuzzy.FuzzyDateTime(
        datetime.datetime(2017, 1, 1, tzinfo=UTC),
        datetime.datetime(2018, 1, 1, tzinfo=UTC))
    delivery_window_end = factory.fuzzy.FuzzyDateTime(
        datetime.datetime(2018, 1, 1, tzinfo=UTC),
        datetime.datetime(2018, 10, 10, tzinfo=UTC))

    class Meta:
        model = DispatchOrder

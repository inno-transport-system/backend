from rest_framework import serializers

from orders.models import *

__all__ = ('OrderSerializer', 'DispatchOrderSerializer')


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = (
            'id', 'status', 'weight', 'number_of_objects', 'creation_time',
            'source_longitude', 'source_latitude',
            'destination_longitude', 'destination_latitude',
            'delivery_window_start', 'delivery_window_end', 'customer_id',
            'source', 'destination'
        )
        extra_kwargs = {
            'id': {'read_only': True},

            'source_longitude': {'required': True},
            'destination_longitude': {'required': True},
            'source_latitude': {'required': True},
            'destination_latitude': {'required': True},
            'delivery_window_start': {'required': True},
            'delivery_window_end': {'required': True},
            'source': {'required': True},
            'destination': {'required': True},
            'customer_id': {'required': True},
        }

    @staticmethod
    def validate_source_longitude(value):
        if not (-180.0 <= value <= 180.0):
            raise serializers.ValidationError(
                "Source longitude violates "
                "the range of valid values")
        return value

    @staticmethod
    def validate_source_latitude(value):
        if not (-90.0 <= value <= 90.0):
            raise serializers.ValidationError(
                "Source latitude violates "
                "the range of valid values")
        return value

    @staticmethod
    def validate_destination_longitude(value):
        if not (-180.0 <= value <= 180.0):
            raise serializers.ValidationError(
                "Destination longitude violates "
                "the range of valid values")
        return value

    @staticmethod
    def validate_destination_latitude(value):
        if not (-90.0 <= value <= 90.0):
            raise serializers.ValidationError(
                "Destination latitude violates "
                "the range of valid values")
        return value


class DispatchOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = DispatchOrder
        fields = (
            'id', 'status', 'order', 'vehicle', 'creation_time',
            'source_longitude', 'source_latitude',
            'destination_longitude', 'destination_latitude',
            'delivery_window_start', 'delivery_window_end',
            'source', 'destination', 'deliver_to',
        )
        extra_kwargs = {
            'id': {'read_only': True},

            'source_longitude': {'required': True},
            'destination_longitude': {'required': True},
            'source_latitude': {'required': True},
            'destination_latitude': {'required': True},
            'delivery_window_start': {'required': True},
            'delivery_window_end': {'required': True},
        }

    @staticmethod
    def validate_source_longitude(value):
        if not (-180.0 <= value <= 180.0):
            raise serializers.ValidationError(
                "Source longitude violates "
                "the range of valid values")
        return value

    @staticmethod
    def validate_source_latitude(value):
        if not (-90.0 <= value <= 90.0):
            raise serializers.ValidationError(
                "Source latitude violates "
                "the range of valid values")
        return value

    @staticmethod
    def validate_destination_longitude(value):
        if not (-180.0 <= value <= 180.0):
            raise serializers.ValidationError(
                "Destination longitude violates "
                "the range of valid values")
        return value

    @staticmethod
    def validate_destination_latitude(value):
        if not (-90.0 <= value <= 90.0):
            raise serializers.ValidationError(
                "Destination latitude violates "
                "the range of valid values")
        return value

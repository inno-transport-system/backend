from rest_framework.routers import DefaultRouter
from django.urls import path
from orders.views import *

app_name = 'orders'

router = DefaultRouter()
router.register(r'dispatch', DispatchOrderViewSet, base_name='dispatch_order')
router.register(r'', OrderViewSet, base_name='order')
urlpatterns = [
    path(r'customer/<int:id>', customer_orders, name='order_list')
]

urlpatterns += router.urls

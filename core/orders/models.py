from uuid import uuid4

from django.db import models
from assets.models import Vehicle
from djchoices import ChoiceItem, DjangoChoices

__all__ = ['Order', 'DispatchOrder']


class Order(models.Model):
    class Status(DjangoChoices):
        new = ChoiceItem('new', 'New')
        validated = ChoiceItem('validated', 'Validated')
        planned = ChoiceItem('planned', 'Planned')
        delivered = ChoiceItem('delivered', 'Delivered')
        canceled = ChoiceItem('canceled', 'Canceled')

    id = models.AutoField(
        verbose_name="ID", primary_key=True)

    weight = models.IntegerField(
        verbose_name="Order weight in grams", null=True)
    number_of_objects = models.IntegerField(
        verbose_name="Number of objects", null=True)
    creation_time = models.DateTimeField(
        verbose_name="Time of creation of order", auto_now_add=True, null=True)
    source_longitude = models.FloatField(
        verbose_name="The longitude of the source", null=True)
    source_latitude = models.FloatField(
        verbose_name="The latitude of the source", null=True)
    source = models.CharField(
        verbose_name='The text address of source',
        null=True, max_length=1024)

    destination = models.CharField(
        verbose_name='The text address of destination',
        null=True, max_length=1024)
    destination_longitude = models.FloatField(
        verbose_name="The longitude of the destination", null=True)
    destination_latitude = models.FloatField(
        verbose_name="The latitude of the destination", null=True)
    delivery_window_start = models.DateTimeField(
        verbose_name="The start of the delivery window", null=True)
    delivery_window_end = models.DateTimeField(
        verbose_name="The end of the delivery window", null=True)
    source = models.CharField(
        verbose_name="Order source address", max_length=1024, null=True)
    destination = models.CharField(
        verbose_name="Order destination address", max_length=1024, null=True)
    status = models.CharField(
        verbose_name="Status", choices=Status.choices,
        default=Status.new, null=False, max_length=256)

    def __repr__(self):
        return f'Order {self.id} - [{self.creation_time}] <{self.id}>'

    def __str__(self):
        return f'{self.id} - [{self.creation_time}]'
    customer_id = models.IntegerField(
        verbose_name="The id of the customer", null=True)


class DispatchOrder(models.Model):
    class Status(DjangoChoices):
        assigned = ChoiceItem('assigned', "Assigned")
        active = ChoiceItem('active', 'In Progress')
        finished = ChoiceItem('finished', 'Finished')

    id = models.AutoField(
        verbose_name="ID", primary_key=True)
    order = models.ForeignKey(
        Order, on_delete=models.CASCADE)

    vehicle = models.ForeignKey(
        Vehicle, on_delete=models.DO_NOTHING, null=True)

    creation_time = models.DateTimeField(
        verbose_name="Time of creation of dispatch order", auto_now_add=True, null=True)
    source_longitude = models.FloatField(
        verbose_name="The longitude of the source", null=True)
    source_latitude = models.FloatField(
        verbose_name="The latitude of the source", null=True)
    destination_longitude = models.FloatField(
        verbose_name="The longitude of the destination", null=True)
    destination_latitude = models.FloatField(
        verbose_name="The latitude of the destination", null=True)

    source = models.CharField(
        verbose_name="Source address", max_length=1024, null=True)
    destination = models.CharField(
        verbose_name="Destination address", max_length=1024, null=True)

    status = models.CharField(
        verbose_name='Status', choices=Status.choices,
        default=Status.assigned, max_length=256)

    delivery_window_start = models.DateTimeField(
        verbose_name="The start of the delivery window", null=True)
    delivery_window_end = models.DateTimeField(
        verbose_name="The end of the delivery window", null=True)

    deliver_to = models.CharField(
        "The name of the recipient", max_length=256, null=True)

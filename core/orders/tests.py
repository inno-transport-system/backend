import datetime

from django.test import TestCase
from rest_framework.serializers import ValidationError

from .models import *
from .serializers import *


class OrdersModelTestCase(TestCase):
    def setUp(self):
        order = Order.objects.create(
            weight=5612, number_of_objects=3, status="validated")
        self.order_id = order.id
        DispatchOrder.objects.create(order=order)

    def test_order_weight(self):
        order_item = Order.objects.get(id=self.order_id)
        self.assertEqual(order_item.weight, 5612)

    def test_order_number_of_objects(self):
        order_item = Order.objects.get(id=self.order_id)
        self.assertEqual(order_item.number_of_objects, 3)

    def test_order_creation_time(self):
        order_item = Order.objects.get(id=self.order_id)
        self.assertEqual(str(order_item.creation_time)[
                         :16], str(datetime.datetime.now())[:16])

    def test_order_status(self):
        order_item = Order.objects.get(id=self.order_id)
        self.assertEqual(order_item.status, "validated")

    def test_dispatch_order(self):
        order_item = Order.objects.get(id=self.order_id)
        disp_ord = DispatchOrder.objects.get(order=order_item)
        self.assertEqual(disp_ord.order, order_item)


class OrdersLocationValidationTestCase(TestCase):
    def test_validate_source_longitude(self):
        with self.assertRaises(ValidationError):
            ser = OrderSerializer(self)
            self.assertEqual(ser.validate_source_longitude(181), 181)

    def test_validate_source_latitude(self):
        ser = OrderSerializer(self)
        self.assertEqual(ser.validate_source_latitude(90), 90)

    def test_validate_destination_longitude(self):
        ser = OrderSerializer(self)
        self.assertEqual(ser.validate_destination_longitude(153), 153)

    def test_validate_destination_latitude(self):
        ser = OrderSerializer(self)
        self.assertEqual(ser.validate_destination_latitude(-48), -48)

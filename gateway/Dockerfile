# collecting npm-managed static files
FROM node:8.11-alpine as npm

WORKDIR /app/customer

COPY ./customer/frontsite/package.json ./
COPY ./customer/frontsite/package-lock.json ./
RUN npm install
RUN mkdir -p static && \
    cp -r node_modules/*/dist/* static/ && \
    cp -r node_modules/*/css static/ && \
    cp -r node_modules/*/fonts static/


# collecting static files
FROM python:3.7 AS statics

RUN pip install --upgrade pip

WORKDIR /app/core

COPY ./core/requirements.txt .
RUN pip install -r requirements.txt
COPY ./core .

RUN python manage.py collectstatic --no-input

WORKDIR /app/customer

COPY ./customer/requirements.txt .
RUN pip install -r requirements.txt
COPY ./customer .

RUN python manage.py collectstatic --no-input


# building gateway
FROM nginx:1.15-alpine

COPY --from=statics /app/core/static /srv/static/core
COPY --from=statics /app/customer/static /srv/static/customer
COPY --from=npm /app/customer/static /srv/static/customer
COPY ./gateway/config /etc/nginx

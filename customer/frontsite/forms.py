from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
import requests


class CreateOrderForm(forms.Form):
    source = forms.CharField(widget=forms.TextInput(attrs={'type': 'search', 'class': 'form-control'}),
                             label="Source address")
    destination = forms.CharField(widget=forms.TextInput(attrs={'type': 'search', 'class': 'form-control'}),
                                  label="Destination address")
    delivery_window_start = forms.DateTimeField(widget=forms.SelectDateWidget(attrs={'class': 'date-control'}),
                                                label='Delivery starts at')
    delivery_window_end = forms.DateTimeField(widget=forms.SelectDateWidget(attrs={'class': 'date-control'}),
                                              label='Delivery deadline')
    source_latitude = forms.FloatField(widget=forms.HiddenInput, label="")
    source_longitude = forms.FloatField(widget=forms.HiddenInput, label="")
    destination_latitude = forms.FloatField(widget=forms.HiddenInput, label="")
    destination_longitude = forms.FloatField(
        widget=forms.HiddenInput, label="")
    weight = forms.IntegerField(label="Weight of a parcel in gramms",
                                widget=forms.NumberInput(attrs={'class': 'form-control'}))
    number_of_objects = forms.IntegerField(label="Amount of objects",
                                           widget=forms.NumberInput(attrs={'class': 'form-control'}))

    def clean(self):
        cleaned_data = super().clean()
        source_longitude = cleaned_data.get("source_longitude")
        source_latitude = cleaned_data.get("source_latitude")
        destination_longitude = cleaned_data.get("destination_longitude")
        destination_latitude = cleaned_data.get("destination_latitude")
        delivery_window_start = cleaned_data.get("delivery_window_start")
        delivery_window_end = cleaned_data.get("delivery_window_end")
        weight = cleaned_data.get("weight")

        if source_longitude is float and not (-180.0 <= source_longitude <= 180.0):
            msg = "Source longitude violates the range of valid values."
            self.add_error('source_longitude', msg)

        if source_latitude is float and not (-90.0 <= source_latitude <= 90.0):
            msg = "Source latitude violates the range of valid values."
            self.add_error('source_latitude', msg)

        if destination_longitude is float and not (-180.0 <= destination_longitude <= 180.0):
            msg = "Destination longitude violates the range of valid values."
            self.add_error('destination_longitude', msg)

        if destination_latitude is float and not (-90.0 <= destination_latitude <= 90.0):
            msg = "Destination latitude violates the range of valid values."
            self.add_error('destination_latitude', msg)

        if delivery_window_start is None:
            msg = "Set up time window"
            self.add_error('delivery_window_start', msg)

        if delivery_window_end is None:
            msg = "Set up time window"
            self.add_error('delivery_window_end', msg)
        if delivery_window_end is not None and delivery_window_start is not None \
                and delivery_window_start >= delivery_window_end:
            msg = "The end of the delivery window should after the start."
            self.add_error('delivery_window_end', msg)

        if weight is None or weight < 0:
            msg = "Weight should be greater than 0"
            self.add_error('weight', msg)

    def save(self):
        fields = self.__dict__['data']
        # headers = []
        payload = {
            "status": 'new',
            "weight": fields['weight'],
            "number_of_objects": fields['number_of_objects'],
            "source_longitude": fields['source_longitude'],
            "source_latitude": fields['source_latitude'],
            "destination_longitude": fields['destination_longitude'],
            "destination_latitude": fields['destination_latitude'],
            "delivery_window_start": fields['delivery_window_start'],
            "delivery_window_end": fields['delivery_window_end']
        }
        url = '/api/v1/orders/'
        r = requests.post(url, payload=payload)
        print("###IGOR HUIGOR" * 3)
        print(r.json())


class SignUpForm(UserCreationForm):
    email = forms.EmailField(
        max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )

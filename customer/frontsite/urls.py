from django.urls import path

from django.contrib.auth import views as auth_views

from .views import *

urlpatterns = [
    path('', navigation, name='customer_main'),
    path('login',
         auth_views.LoginView.as_view(
         template_name='user/customer_login.html', extra_context={'user': None}),
         name='user-login'),
    path('logout',
         auth_views.LogoutView.as_view(next_page='/'),
         name='user-logout'),
    path('create-order', create_order, name='customer_create'),
    path('register', register, name='customer_register'),
    path('order-list', order_list, name='customer_order_list')
]

from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from .forms import CreateOrderForm, SignUpForm
from customer.settings import SERVICE_CORE
from django.contrib.auth.decorators import login_required, permission_required

import requests
import json
import datetime
import time


def navigation(request):
    return render(request, 'customer.html', {'user': request.user})
    # Проблема в том, что имена могут у %subapp%/jinja2/ пересекаться и возьмется из frontsite


@login_required
def create_order(request):
    if request.method == 'POST':
        form = CreateOrderForm(request.POST)
        if form.is_valid():
            url = 'http://{}:80/api/v1/orders/'.format(SERVICE_CORE)

            # data = json.loads(form.cleaned_data)
            utc_offset_sec = time.altzone if time.localtime().tm_isdst else time.timezone
            utc_offset = datetime.timedelta(seconds=-utc_offset_sec)
            data = form.cleaned_data

            start = data['delivery_window_start']
            end = data['delivery_window_end']
            start = start.replace(tzinfo=datetime.timezone(
                offset=utc_offset)).isoformat()
            end = end.replace(tzinfo=datetime.timezone(
                offset=utc_offset)).isoformat()
            start = start[:start.index('+')] + 'Z'
            end = end[:end.index('+')] + 'Z'

            data['delivery_window_start'] = start
            data['delivery_window_end'] = end
            data['customer_id'] = request.user.id
            print('MY DATA = {}'.format(data))

            response = requests.post(url, json=data)
            if response.status_code == 201:
                return redirect('customer_main')
            else:
                return render(request, 'customer_create.html', {'form': CreateOrderForm(),
                                                                'error': 'Error during internal query!',
                                                                'user': request.user})
    else:
        form = CreateOrderForm()
    return render(request, 'customer_create.html', {'form': form, 'error': None, 'user': request.user})

def get_date(date):
    result = date
    for i in range(len(date)):
        if date[i]=='T':
            result = date[:i] + " "+ date[i+1:]
        if date[i]=='.':
            result = result[:i]
            break

    return result

@login_required
def order_list(request):
    url = 'http://{}:80/api/v1/orders/customer/{}'.format(SERVICE_CORE, request.user.id)
    response = requests.get(url)
    ans = json.loads(response.text)
    print("THIS IS ORDERS")
    print(ans)

    for order in ans['orders']:
        order['creation_time'] = get_date(order['creation_time'])
    return render(request, 'customer_lookup.html', {'orders': ans['orders'], 'user': request.user})


def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('customer_main')
    else:
        form = SignUpForm()

    return render(request, 'user/customer_registration.html', {'form': form, 'user': request.user})

class Map {
    constructor(initial_points, center, container = "mapContainer", draggable = false) {
        console.log("I started to init map")
        this.platform = new H.service.Platform({
            'app_id': 'hEjGojDqjfT0V0BqFrlN',
            'app_code': 'yjQT3AGFD_wdfHMBysfTRQ'
        });

        const defaultLayers = this.platform.createDefaultLayers();
        this.rectangle_color = 'white'
        this.letter_color = 'orange'
        this.here_map = new H.Map(
            document.getElementById(container),
            defaultLayers.normal.map,
            {}
        );

        this.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.here_map));
        this.ui = H.ui.UI.createDefault(this.here_map, defaultLayers);
        this.draggable = draggable

        if (typeof center === 'undefined') {
            let markers = []
            for (let i = 0; i < initial_points.length; i++)
                markers.push(this.create_marker_nameless(initial_points[i]));

            let group = new H.map.Group();
            group.addObjects(markers);
            this.here_map.setViewBounds(group.getBounds());
            this.here_map.addObject(group); // TODO: PREVIOUSLY I ALREADY ADD  ITEMS< BUT THEY DO NOT WISH TO APPEAR
        } else {
            this.move_map(center)

            if (initial_points.length === 2) {
                this.add_initial(initial_points)
            }
        }

        console.log("I initialized map")
    };

    get_markers_amount() {
        let result = this.get_markers();
        return typeof result === 'undefined' ? 0 : result.length;
    };

    add_initial(points) {
        let marker_src = this.create_marker(points[0], 'S', false);
        let marker_dst = this.create_marker(points[1], 'D', false);
        this.add_marker(marker_src);
        this.add_marker(marker_dst);
    };

    get_markers() {
        let markers = [];
        const items = this.here_map.getObjects();
        for (let i = 0; i < items.length; i++)
            if (items[i] instanceof H.map.Marker)
                markers.push(items[i]);
        return markers;
    };

    create_marker_nameless(point) {
        const amount = this.get_markers_amount();
        return this.create_marker(point, `${amount}`)
    };

    create_marker(point, letter, draggability) {
        const marker = new H.map.Marker(point, {icon: this.create_icon(letter)});

        if (typeof draggability !== 'undefined') {
            marker.draggable = draggability;
        } else {
            marker.draggable = this.draggable;
        }
        this.add_marker(marker);
        return marker;
    };

    get_coord_by_id(marker_id) {
        marker_id = parseInt(marker_id);
        const objects = this.get_markers();
        let marker = NaN;
        for (let i = 0; i < objects.length; i++) {
            var item_id = objects[i].getId();
            if (item_id === marker_id) {
                marker = objects[i];
                break
            }
        }

        if (marker === NaN)
            return NaN;
        return marker.getPosition()
    }

    add_marker(marker) {
        // Marker is an H.map.Marker object
        console.log("Adding marker");
        this.here_map.addObject(marker)
    };

    move_map(point) {
        // should be a dictionary of lat, lng
        this.here_map.setCenter(point);
        this.here_map.setZoom(5);
    };

    render_markup(rec_color, let_color, letter) {
        var markup = '<svg  width="24" height="24" xmlns="http://www.w3.org/2000/svg">' +
            `<rect stroke="black" fill="${rec_color}" x="1" y="1" width="22" height="22" />` +
            '<text x="12" y="18" font-size="12pt" font-family="Arial" font-weight="bold" ' +
            `text-anchor="middle" fill="${let_color}" > ${letter}</text></svg>`
        return markup
    };

    create_icon(letter) {
        const svg_markup = this.render_markup(this.rectangle_color, this.letter_color, letter)
        let icon = new H.map.Icon(svg_markup)
        return icon
    };
}


class CreationMap extends Map {
    constructor(initial_points, center, container = "mapContainer", draggable = true, async) {
        super(initial_points, center, container, draggable);
        this.async = async;
        this.add_listeners();
    }

    add_listeners() {
        this.add_draggability()
    }

    add_draggability() {
        let link = this;
        // disable the default draggability of the underlying map
        // when starting to drag a marker object:
        this.here_map.addEventListener('dragstart', function (ev) {
            let target = ev.target;
            if (target instanceof H.map.Marker) {
                link.behavior.disable();
            }
        }, false);

        // re-enable the default draggability of the underlying map
        // when dragging has completed
        this.here_map.addEventListener('dragend', function (ev) {
            let target = ev.target;
            if (target instanceof mapsjs.map.Marker) {
                link.behavior.enable();
            }
        }, false);

        // Listen to the drag event and move the position of the marker
        // as necessary
        this.here_map.addEventListener('drag', function (ev) {
            let target = ev.target,
                pointer = ev.currentPointer;
            if (target instanceof mapsjs.map.Marker) {
                target.setPosition(link.here_map.screenToGeo(pointer.viewportX, pointer.viewportY));
            }
        }, false);
    }

    create_marker_with_id(point) {
        const amount = this.get_markers_amount() - 1;
        return this.create_marker(point, `${amount}`)
    };

    geocode(query) {
        const link = this;
        function onSuccess(result) {
            let locations = result.response.view[0].result;
            console.log("I am in on success function");
            console.log(locations);
            let location = locations[0]['location']['displayPosition'];
            let point = {'lat': location['latitude'], 'lng': location['longitude']};
            let marker = link.create_marker_with_id(point);
            link.move_map(point);
            link.async(query, marker.getId());
        }

        function onError(result) {
            console.log("Life sucks");
        }

        let geocoder = this.platform.getGeocodingService(),
            geocodingParameters = {
                searchText: query,
                jsonattributes: 1
            };

        geocoder.geocode(
            geocodingParameters,
            onSuccess,
            onError
        );
    };
}


class OrderCreationMap extends Map {
    constructor(initial_points, center, container = "mapContainer", draggable = true, action, async) {
        super(initial_points, center, container, draggable);
        this.action_click = action;
        this.async = async;
        this.markers = [null, null];
        this.add_listeners();
    }

    add_listeners() {
        // this.set_point();
        this.add_draggability()
    }

    set_point() {
        let link = this;
        this.here_map.addEventListener('tap', function (evt) {
            let coord = link.here_map.screenToGeo(evt.currentPointer.viewportX, evt.currentPointer.viewportY);

            if (link.get_markers_amount() < 2) {
                let marker;
                if (link.get_markers_amount() === 0)
                    marker = new H.map.Marker(coord, {icon: link.create_icon('S')});
                else
                    marker = new H.map.Marker(coord, {icon: link.create_icon('D')});
                marker.draggable = link.draggable;
                link.add_marker(marker);
                link.action_click(marker.getId(), marker.getPosition(), 'set_point')
            }
        });
    }

    add_draggability() {
        // disable the default draggability of the underlying map
        // when starting to drag a marker object:
        let link = this;

        this.here_map.addEventListener('dragstart', function (ev) {
            let target = ev.target;
            if (target instanceof H.map.Marker) {
                link.behavior.disable();
            }
        }, false);

        // re-enable the default draggability of the underlying map
        // when dragging has completed
        this.here_map.addEventListener('dragend', function (ev) {
            let target = ev.target;
            if (target instanceof mapsjs.map.Marker) {
                link.behavior.enable();
                // marker_pos = target.getPosition();
                // marker_id = target.getId();
                // update_dropdown(marker_id, 'update', marker_pos);
            }
        }, false);

        // Listen to the drag event and move the position of the marker
        // as necessary
        this.here_map.addEventListener('drag', function (ev) {
            let target = ev.target,
                pointer = ev.currentPointer;
            if (target instanceof mapsjs.map.Marker) {
                target.setPosition(link.here_map.screenToGeo(pointer.viewportX, pointer.viewportY));
                link.action(target.getId(), target.getPosition(), 'set_point')
            }
        }, false);
    }

    geocode(query, source_id) {
        const link = this;
        function onSuccess(result) {
            var locations = result.response.view[0].result;
            console.log("I am in on success function");
            console.log(locations);
            let location = locations[0]['location']['displayPosition'];
            let point = {'lat': location['latitude'], 'lng': location['longitude']};
            if (source_id === 'id_source'){
                if(link.markers[0] !== null)
                    link.here_map.removeObject(link.markers[0]);
                link.markers[0] = link.create_marker(point, 'S', false);
            } else {
                if(link.markers[1] !== null)
                    link.here_map.removeObject(link.markers[1]);
                link.markers[1] = link.create_marker(point, 'D', false);
            }
            link.move_map(point);
            link.async(point, source_id);
        }

        function onError(result) {
            console.log("Life sucks");
        }

        var geocoder = this.platform.getGeocodingService(),
            geocodingParameters = {
                searchText: query,
                jsonattributes: 1
            };

        geocoder.geocode(
            geocodingParameters,
            onSuccess,
            onError
        );
    };
}

class VehicleMap extends Map {
    constructor(initial_points, center, container = "mapContainer", draggable = false) {
        super(initial_points, center, container, draggable);
    }

    render_markup(rec_color, let_color, letter) {
        var markup = '<svg width="24.0" height="24.0" xmlns="http://www.w3.org/2000/svg" >'
            + '    <!-- Top -->'
            + '      <rect x="4.602739726027397" y="0.6575342465753424" width="14.465753424657533" height="8.54794520547945" fill="transparent" rx="9.863013698630137" stroke="crimson" stroke-width="0.6575342465753424" />'
            + '    '
            + '    <!-- Body -->'
            + '      <rect x="0.6575342465753424" y="4.602739726027397" width="22.35616438356164" height="5.260273972602739" fill="crimson" rx="1.9726027397260273" />'
            + '      '
            + '    <g>'
            + '    <!-- Left line -->'
            + '      <line x1="9.534246575342465" y1="0.6575342465753424" x2="9.534246575342465" y2="5.260273972602739" stroke="crimson" stroke-width="0.6575342465753424"/>'
            + ''
            + '    <!-- Right line -->'
            + '      <line x1="14.136986301369863" y1="0.6575342465753424" x2="14.136986301369863" y2="5.260273972602739" stroke="crimson" stroke-width="0.6575342465753424"/>'
            + '    </g>'
            + '  '
            + '    <g>'
            + '    <!-- Left bumper -->'
            + '      <rect x="0.0" y="7.232876712328767" width="2.6301369863013697" height="1.3150684931506849" fill="#999" rx="0.6575342465753424" />'
            + '    '
            + '    <!-- Right bumper -->'
            + '      <rect x="21.36986301369863" y="7.232876712328767" width="2.6301369863013697" height="1.3150684931506849" fill="#999" rx="0.6575342465753424" />'
            + '    </g>  '
            + '  '
            + '    <!-- Left wheel -->'
            + '    <g>'
            + '      <circle r="2.6301369863013697px" fill="#222" stroke="white" stroke-width="0.4602739726027397" cx="5.917808219178082" cy="9.205479452054794"/>    '
            + '      <circle r="0.9863013698630136px" fill="#555" cx="5.917808219178082" cy="9.205479452054794"/>'
            + '    </g>'
            + '  '
            + '    <!-- Right wheel -->'
            + '    <g>'
            + '      <circle r="2.6301369863013697px" fill="#222" stroke="white" stroke-width="0.4602739726027397" cx="17.753424657534246" cy="9.205479452054794"/>'
            + '      <circle r="0.9863013698630136px" fill="#555" cx="17.753424657534246" cy="9.205479452054794"/>'
            + '    </g>  '
            + ''
            + '    <g>'
            + '    <!-- Gold light -->'
            + '      <circle r="0.9863013698630136px" fill="gold" cx="22.35616438356164" cy="5.917808219178082"/>'
            + '      '
            + '    <!-- Orange light -->'
            + '      <circle r="0.6575342465753424px" fill="orange" cx="0.9863013698630136" cy="5.917808219178082"/>'
            + '    </g>  '
            + '</svg>    '

        return markup
    };


};

Array.prototype.contains = function (v) {
    for (let i = 0; i < this.length; i++) {
        if (this[i] === v) return true;
    }
    return false;
};

Array.prototype.unique = function () {
    let arr = [];
    for (let i = 0; i < this.length; i++) {
        if (!arr.includes(this[i])) {
            arr.push(this[i]);
        }
    }
    return arr;
};
